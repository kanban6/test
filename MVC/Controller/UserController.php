<?php

namespace MVC\Controller;

use system\Controller;

use MVC\Model\User;

use system\Router;

class UserController extends Controller
{
	
	public function homeAction()
	{
		$this->logStatus();

		$user = new User();

		$title = 'Користувач ' . $user->login;
		
		$this->setContent('title', $title);

		$this->setContent('user', $user);

		$this->View('User/home', 'user');
	}

	private function logStatus()
	{
		
		$user = new User();
		
		$status = $user->logStatus();

		$url = Router::getUrl();
		
		if ($status == false && $url != '') {

			header("Location: /login");	
			die('You are not logged in');
		}
		
	}

	public function listTestAction()
	{

		$this->logStatus();

		$user = new User();

		$title = 'Список тестів ';
		
		$this->setContent('title', $title);

		$this->setContent('user', $user);

		$listTest = $user->getListTest();

		$this->setContent('listTest', $listTest);

		$this->View('User/listTest', 'user');


	}

	public function logoutAction()
	{
		$user = new User();

		$user->logOut();
		header("Location: /login");	
	}
	
}