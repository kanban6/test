<?php

namespace MVC\Controller;

use system\Controller;
use system\Router;
use MVC\Model\Admin;

/**
* 
*/
class AdminController extends Controller
{
	
	/**
	 * [logStatus description]
	 * @return [type] [description]
	 */
	private function logStatus()
	{
		
		$admin = Admin::getInstance();
		
		$status = $admin->logStatus();
		
		if ($status == false) {

			header("Location: /admin");	
			die('You are not logged in');
		}
		
	}


	/**
	 * [loginAction description]
	 * @return [type] [description]
	 */
	public function loginAction()
	{

		$admin = Admin::getInstance();

		if (isset($_POST['login']) && isset($_POST['password'])) {
		
			$result = $admin->logIn($_POST['login'], $_POST['password']);

			if ($result) {

				header("Location: /admin/home");	
				
			}else {

				$error = 'Логін або пароль невірні';
				$this->setContent('error', $error);
			}

		}

		$title = 'Вхід - Адмін панель';
		
		$this->setContent('title', $title);
		$this->View('Admin/login', 'default');

	}


	/**
	 * [homeAction description]
	 * @return [type] [description]
	 */
	public function homeAction()
	{
		$this->logStatus();
		$title = 'Адмін панель';
		
		$this->setContent('title', $title);
		$this->View('Admin/home', 'admin');
	}


	/**
	 * [logoutAction description]
	 * @return [type] [description]
	 */
	public function logoutAction()
	{
		$admin = Admin::getInstance();

		$admin->logOut();
		header("Location: /admin");	
	}

	public function addTestAction()
	{

		$this->logStatus();
		$title = 'Додати тест';
		$this->setContent('title', $title);
		$this->View('Admin/addTest', 'admin');

		if (isset($_POST['testName'])) {

			$admin = Admin::getInstance();
			
			$idTest = $admin->addTest($_POST);
 
			header("Location: /admin/add-question/$idTest");

		}
	}

	public function addQuestionAction()
	{
		$this->logStatus();
		$title = 'Додати запитання';
		$this->setContent('title', $title);
		$this->View('Admin/addQuestion', 'admin');

		if (isset($_POST['submit'])) {

			unset($_POST['submit']);

			$admin = Admin::getInstance();
			
			$admin->addQuestion($_POST);
		}
	}

	public function getListAction()
	{
		$this->logStatus();
		$title = 'Список тестів';
		$this->setContent('title', $title);
		

		$admin = Admin::getInstance();
		$testList = $admin->getListTest();

		$this->setContent('list', $testList);


		$this->View('Admin/testList', 'admin');
	}
}