<?php

namespace MVC\Controller;

use system\Controller;

use MVC\Model\User;

use system\Router;

class PagesController extends Controller
{
	
	public function homeAction()
	{
		
		$this->logStatus();

		$title = 'Головна - реєстрація';
		
		$this->setContent('title', $title);

		if (isset($_POST['login']) && isset($_POST['password']) && isset($_POST['repeat_password'])) {
			
			if ($_POST['password'] === $_POST['repeat_password']) {
				
				$user = new User();
				$staus = $user->registration($_POST['login'], $_POST['password']);

				if ($staus == false) {
					
					$error = 'Такий логін існує';
					$this->setContent('error', $error);
				}else{

					header("Location: /login");	
				}

			}else {


				$error = 'Паролі не співпадають';
				$this->setContent('error', $error);
			}
		}

		$this->View('home');
	}


	public function loginUserAction()
	{

		$this->logStatus();
		$title = 'Вхід';
		
		$this->setContent('title', $title);

		if (isset($_POST['login']) && isset($_POST['password'])) {
		
			$user = new User();

			$result = $user->logIn($_POST['login'], $_POST['password']);

			if ($result == false) {

				$error = 'Логін або пароль невірні';
				$this->setContent('error', $error);
				
			}else {
				
				header("Location: /user/home");	
			}

		}

		$this->View('loginUser');

	}


	private function logStatus()
	{
		
		$user = new User();
		
		$status = $user->logStatus();

		$url = Router::getUrl();
		
		if ($status == true && $url = '' || $url = '/login') {

			header("Location: /user/$user->id");	
			die('You are not logged in');
		}
		
	}
	
}