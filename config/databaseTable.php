<?php

/**
 * array tables databese table=>field=>type 
 */
return [

	'tests' => [

			'id'       => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'testName' => 'varchar(150) NOT NULL'
	],	

	'question' => [

			'id'         => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'title'      => 'varchar(250) NOT NULL',
			'idTest'     => 'int(6) NOT NULL',
			'answer1'    => 'varchar(250) NOT NULL',
			'answer2'    => 'varchar(250) NOT NULL',
			'answer3'    => 'varchar(250) NOT NULL',
			'answer4'    => 'varchar(250) NOT NULL',
			'trueAnswer' => 'int(1) NOT NULL',
			'bal'        => 'int(4) NOT NULL'
	],	

	'users'   => [

			'id'    => 'int(6) AUTO_INCREMENT KEY NOT NULL',
			'login'  => 'varchar(20) NOT NULL',
			'password'  => 'varchar(60) NOT NULL'
	],

	'admin'   => [

			'id'    => 'int(1) AUTO_INCREMENT KEY NOT NULL',
			'login'  => 'varchar(20) NOT NULL',
			'password'  => 'varchar(60) NOT NULL'

	]


];