<?php

namespace system;

/**
 * Router control class
 * pattern singleton
 */
class Router
{

	/**
     * $instance object
     * @var null
     */
    private static $instance = null;
    
    /**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
          
            self::$instance = new self();
           
        }
        
        return self::$instance;
    }

	/**
	 * getUrl return url address
	 * @return string
	 */
	public static function getUrl()
	{
		$url = trim($_SERVER['REQUEST_URI'], '/');

		return $url;

	}

	/**
	 * route control urls method
	 * create object Controller
	 */
	public function route()
	{

		$url = self::getUrl();
		$controller = null;
		$urlRouts = Config::getConfig('urlRouts', 'urls');

		foreach ($urlRouts as $rout => $value) {
			if ($url == $rout) {
				$controller = $value['controller'] . 'Controller';
				$action = $value['action'] . 'Action';
			}
		}

		$urlPattern = Config::getConfig('urlRouts', 'pattern');

		foreach ($urlPattern as $pattern => $value) {

			if (preg_match("/$pattern/", $url)) {
				$controller = $value['controller'] . 'Controller';
				$action = $value['action'] . 'Action';
			}
		}
		
		
		if (class_exists($controller) && method_exists($controller, $action)) {
			 $controller = new $controller();
			 $controller->$action();
		} else {

			Controller::View('404');
		}
		
	}

}

