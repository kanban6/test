<?php

/**
 * array with url route
 */
return [

    'urls' => [
        ''        => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'home'
        ],
        'login' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'loginUser'
        ],
        'admin' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'login'
        ],
        'admin/home' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'home'
        ],
        'admin/logout' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'logout'
        ],
        'admin/add-test' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'addTest'
        ],
        'admin/list-test' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'getList'
        ],
        'user/logout' => [
            'controller' => 'MVC\Controller\User',
            'action'     => 'logout'
        ],
        'user/list-test' => [
            'controller' => 'MVC\Controller\User',
            'action'     => 'listTest'
        ],
        'user/home' => [
            'controller' => 'MVC\Controller\User',
            'action'     => 'home'
        ],                  
   
    ],

    'pattern' => [

        'admin\/add-question\/[0-9]+' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'addQuestion'
        ]
 
    ]
  
];