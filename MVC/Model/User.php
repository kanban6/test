<?php

namespace MVC\Model;

use system\session\Session;

use system\database\Database;

use system\Controller;

use system\Router;

/**
* 
*/
class User
{

	
    private $session;

    private $db;

    public $id;

    public $login;

    public function __construct()
    {
        
        $this->session = Session::getInstance();

        $this->db = Database::getInstance();

        $this->setInfoUser();
       
    }


    public function logIn($login, $password)
    {
        $logged = false;

        if (strlen($login) < 12 && strlen($password) < 12)  {

            $sql = "SELECT * FROM users WHERE login='$login'";

            $result = $this->db->query($sql);

            if ($result) {
            
                $row = $result->fetch_assoc();
           
                if (password_verify($password, $row['password'])) {

                    $id = $row['id'];
                                    
                    $this->session->setSession("$id");

                    $logged =  true;

                } 
            }
        }

        return $logged;
    	
    }


    public function logStatus()
    {
        $logStatus = false;
        $status = $this->session->getSession();

        if (!$status  == false) {

           $logStatus = true;
        }

           return $logStatus;

    }


    public function logOut()
    {
        $status = $this->session->getSession();

        if (!$status  == false) {

            $this->session->clear();

        }

    }

    private function setInfoUser()
    {
        $id = $this->session->getSession();

        $sql = "SELECT * FROM users WHERE id='$id'";

        $result = $this->db->getRow($sql);

        if ($result != NULL) {
           
           $this->id = $result['id'];;

           $this->login = $result['login'];
        }
    }

    public function registration($login, $password)
    {
        $sql = "SELECT * FROM users WHERE login='$login'";

        $result = $this->db->getRow($sql);

        if ($result != NULL) {
            
            return false;
        }

        $password = password_hash($password, PASSWORD_DEFAULT);
            
        $sql = "INSERT INTO users (login, password) VALUES ('$login', '$password')";

        $this->db->query($sql);

        return true;

    }


    public function getListTest()
    {
        $sql = "SELECT * FROM tests";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_assoc()) {

            $listTest[] = $row;
        
        }

        return $listTest;

    }




}