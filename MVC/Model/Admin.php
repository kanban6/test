<?php

namespace MVC\Model;

use system\session\Session;

use system\database\Database;

use system\Controller;

use system\Router;

/**
* 
*/
class Admin 
{

	/**
     * $instance object
     * @var null
     */
    private static $instance = null;

    private $session;

    private $db;

	/**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self();

            self::$instance->session = Session::getInstance();

            self::$instance->db = Database::getInstance();
        }
        
        return self::$instance;
    }


    public function logIn($login, $password)
    {
        $logged = false;

        if (strlen($login) < 12 && strlen($password) < 12)  {

            $sql = "SELECT * FROM admin WHERE login='$login'";

            $result = $this->db->query($sql);

            if ($result) {
            
                $row = $result->fetch_assoc();
           
                if (password_verify($password, $row['password'])) {
                
                    $this->session->setSession('logged');

                    $logged =  true;

                } 
            }
        }

        return $logged;
    	
    }


    public function logStatus()
    {
        $logStatus = false;
        $status = $this->session->getSession();

        if ($status  == 'logged') {

           $logStatus = true;
        }

           return $logStatus;

    }


    public function logOut()
    {
        $status = $this->session->getSession();

        if ($status  == 'logged') {

            $this->session->clear();

        }

    }


    public function addTest($value = [])
    {
        $idTest = false;

        $this->db->insert('tests', $value);

        $testName = $value['testName'];

        $sql = "SELECT * FROM tests WHERE testName='$testName'";

        $result = $this->db->getRow($sql);

        $idTest = $result['id'];

        return $idTest;
    }


    public function addQuestion($value)
    {
        $url = Router::getUrl();
        $arrayUrl = explode('/', $url);
        $value['idTest'] =  $arrayUrl[2];
        $this->db->insert('question', $value);
    }

    public function getListTest()
    {
        $sql = "SELECT * FROM tests";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_assoc()) {

            $listTest[] = $row['testName'];
        
        }

        return $listTest;

    }

	private function __clone() {}
    private function __construct() {}



}