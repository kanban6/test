<?php

namespace system;

/**
 * class Config
 * include config file
 */
class Config
{
	/**
	 * $cache config array
	 * @var array
	 */
	private static $cache = [];

	/**
	 * getConfig include array file and return config
	 * @param  string $file | file name
	 * @param  boolean $key | array key
	 * @return array        | config array
	 */
	public static function getConfig ($file, $key = false)
	{

		if (!isset(self::$cache[$file])) {
			
			self::$cache[$file] = include 'config/' . $file . '.php';
		}

		$values = self::$cache[$file];

        if (isset($values[$key])) {

                $values = $values[$key];
            } 
        
        return $values;
		
	}
}